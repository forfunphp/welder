<?
namespace LU\Helpers;


use Bitrix\Currency\CurrencyManager;
use Bitrix\Main\Application;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Context;
use Bitrix\Main\Loader;
use Bitrix\Main\SystemException;
use Bitrix\Main\Type\DateTime;
use Bitrix\Main\UserTable;
use Bitrix\Sale\Basket;
use Bitrix\Sale\Order;
use YandexCheckout\Request\Payments\CreatePaymentResponse;
use function Sodium\add;


\Bitrix\Main\Loader::includeModule("sale");
\Bitrix\Main\Loader::includeModule("catalog");
\Bitrix\Main\Loader::includeModule('iblock');

class Loyalty {

	public static function numberRefubActivate()
	{
		global $USER;

		$rsUser = \CUser::GetByID($USER->GetID());
		$arUser = $rsUser->Fetch();
				
		if(!$arUser[UF_NUMBER_REFUB]){
			return true;
		}
	}

	public static function nextLevelUser()
	{		
		global $USER;
		$discount = \LU\Helpers\Loyalty::checkLoyaltyUser();

			$dbRes = \Bitrix\Sale\Order::getList([


			    'filter' => [
			    	"USER_ID" => $USER->GetID(),
			        '!BASKET.PRODUCT_ID' => array("6307", "1628"),
			        '!BASKET.PRICE_TYPE_ID' =>4,
			         'PAYED' => 'Y',
			      
			    ],
			    'order' => ['ID' => 'DESC']
			]);


			$t_3month=7889229;

		   $dbRes1 = \Bitrix\Sale\Order::getList([

		    'filter' => [
		        "USER_ID" => $USER->GetID(),
		        '!BASKET.PRODUCT_ID' => array("6307", "1628"),
		        '!BASKET.PRICE_TYPE_ID' =>4,
		         'PAYED' => 'Y',
		         
		    ],
		    'order' => ['ID' => 'ASC']
			]);

			// Получение даты первого заказа
			if ($order1 = $dbRes1->fetch())
			{			

			// Сколько времени прошло с первого заказа в unix

			$how_time = strtotime('now')-strtotime($order1['DATE_INSERT']);
			$now_time = strtotime('now');
			$how_time_first = strtotime('now')-strtotime($order1['DATE_INSERT']);
		
			}

			$t_3month=7889229;
		
			$arr = array();
			while ($order = $dbRes->fetch())
			{
			        $arr[$order['ID']] = $order['ID'];
			   

			}
			$orders_count = count($arr);


			$rsUser = \CUser::GetByID($USER->GetID());
			$arUser = $rsUser->Fetch();

			if($arUser[UF_4LEVEL_DATE]){
					
					$di = 20;
				}elseif($arUser[UF_3LEVEL_DATE]){
					
					$how_time=strtotime($arUser[UF_3LEVEL_DATE]);
			
				}elseif ($arUser[UF_2LEVEL_DATE]) {
					$how_time=strtotime($arUser[UF_2LEVEL_DATE]);
					
					
				}elseif ($arUser[UF_1LEVEL_DATE]) {
					$how_time=strtotime($arUser[UF_1LEVEL_DATE]);
					
					
				}elseif($arUser[UF_LOYALTY_ACTIVATE]){
					$how_time=strtotime('now')-$how_time_first;
					
				}				


			$d = new \DateTime();
			$d->setTimestamp($how_time);
			$date_from = $d->format('d.m.Y H:i:s');

			$tr = date('d.m.Y H:i:s');
				
		    $dbRes2 = \Bitrix\Sale\Order::getList([

		    'filter' => [
		        "USER_ID" => $USER->GetID(),
		        '!BASKET.PRODUCT_ID' => array("6307", "1628"),
		        '!BASKET.PRICE_TYPE_ID' =>4,
		         'PAYED' => 'Y',			         
		         '>=DATE_INSERT' => $date_from,
		         '<=DATE_INSERT' => date('d.m.Y H:i:s'),
		         	    ],
		    'order' => ['ID' => 'ASC']
			]);

			 $remain_time = '';
			// Получение даты первого заказа на уровне
					
			if($discount == 15){
				$level = 3;
				$next_level = 4;
				$next_discount = 20;
				$all_orders = 21;
				$remain = $all_orders - $orders_count;
				
				
			}elseif($discount == 20){
				$level = 4;
			}elseif($discount == 10){
				$level = 2;
				$next_level = 3;
				$next_discount = 15;
				$all_orders = 16;
				$remain = $all_orders - $orders_count;
				
				
			}elseif ($discount == 5) {
				$level = 1;
				$next_level = 2;
				$next_discount = 10;
				$all_orders = 11;
				$remain = $all_orders - $orders_count;
		
			}elseif ($discount == 0) {
				$level = 0;
				$next_level = 1;
				$next_discount = 5;
				$all_orders = 6;
				$remain = $all_orders - $orders_count;						
			}

			if($remain < 0){
						$remain = 0;
					}

			if($remain_time < 0){
					$remain_time = 0;
				}

			if ($order2 = $dbRes2->fetch())
			{
									
			// Сколько времени прошло с первого заказа в unix
			$how_time = strtotime('now')-strtotime($order2['DATE_INSERT']);
			$t_3month=7889229;
			$remain_time = $t_3month - $how_time;
 
 			
			}else{
				
				$remain_time = false;
				$date_uplevel = false;
			}

			$date_uplevel = (int)$now_time+(int)$remain_time;

			return array($next_discount, $remain, $orders_count, $all_orders, $remain_time, $date_uplevel, $how_time, $level, $discount, $next_level);
			}




	public static function AutoNextLevelUser()
	{

	$filter = Array("ACTIVE" => "Y", "!UF_LOYALTY_ACTIVATE" => false);
	$arUsers = \CUser::GetList($by="ID", $order="desc", $filter,array("SELECT"=>array("UF_LOYALTY_ACTIVATE"), "FIELDS"=>array("ID")));



	while ($user = $arUsers->fetch())
		{
		    global $USER;

			$USER_ID = $user['ID'];
			

			$rsUser = \CUser::GetByID($USER_ID);
			$arUser = $rsUser->Fetch();
		
			$dbRes = \Bitrix\Sale\Order::getList([
				    'filter' => [
				    	"USER_ID" => $USER_ID,
				        '!BASKET.PRODUCT_ID' => array("6307", "1628"),
				        '!BASKET.PRICE_TYPE_ID' =>4,
				         'PAYED' => 'Y',
				      	],
				    'order' => ['ID' => 'DESC']
				]);

			// Подсчет полученных заказов
			$arr = array();
				while ($order = $dbRes->fetch())
				{
				    $arr[$order['ID']] = $order['ID'];
				}

			$orders_count = count($arr);
			

			// Проверка на текущий уровень и количества заказов для перехода на следующий
			if($arUser[UF_4LEVEL_DATE]){
				
				$discount = 20;
			}elseif($arUser[UF_3LEVEL_DATE] && $orders_count > 19){
				$discount = 15;
				$how_time=strtotime($arUser[UF_3LEVEL_DATE]);
		
			}elseif ($arUser[UF_2LEVEL_DATE] && $orders_count > 14) {
				$how_time=strtotime($arUser[UF_2LEVEL_DATE]);
				$discount = 10;
				
			}elseif ($arUser[UF_1LEVEL_DATE] && $orders_count > 10) {
				$how_time=strtotime($arUser[UF_1LEVEL_DATE]);
				$discount = 5;
				
			}elseif($arUser[UF_LOYALTY_ACTIVATE]  && $orders_count > 4){
				$how_time=strtotime($arUser[UF_LOYALTY_ACTIVATE]);
				$discount = '0';
								
			}
				
			\LU\Helpers\Loyalty::remainTimeAndOrders($how_time, $discount, $USER_ID);
			
			}

		return "\LU\Helpers\Loyalty::AutoNextLevelUser();";

	}


	public static function remainTimeAndOrders($how_time, $discount, $USER_ID)
	{			
	$t_3month=7889229;

	if($discount == '0'){
	 $dbRes = Order::getList([

		    'filter' => [
		        "USER_ID" => $USER_ID,
		        '!BASKET.PRODUCT_ID' => array("6307", "1628"),
		        '!BASKET.PRICE_TYPE_ID' =>4,
		         'PAYED' => 'Y',
		         
		    ],
		    'order' => ['ID' => 'DESC']
		]);

		// Получение даты первого заказа
		if ($order = $dbRes->fetch())
		{
		
		$how_time = strtotime('now')-strtotime($order['DATE_INSERT']);

		}

	}


	// Можно через setTimestamp
	$d = new \DateTime();
	$d->setTimestamp($how_time);
	$date_from = $d->format('d.m.Y H:i:s');

	   $dbRes1 = Order::getList([

		    'filter' => [
		        "USER_ID" => $USER_ID,
		        '!BASKET.PRODUCT_ID' => array("6307", "1628"),
		        '!BASKET.PRICE_TYPE_ID' =>4,
		         'PAYED' => 'Y',			         
		         '>=DATE_INSERT' => $date_from,
		         '<=DATE_INSERT' => date('d.m.Y H:i:s'),
		         	    ],
		    'order' => ['ID' => 'ASC']
		]);

	
	if ($order1 = $dbRes1->fetch())
	{

	$remain_time = strtotime('now')-strtotime($order1['DATE_INSERT']);
		
	}


	if($t_3month < $remain_time){

		switch ($discount) {
			case '20':
				break;
			case '15':
			$next_level= '4';
			$arGroup = 14;
				break;
			case '10':
			$next_level= '3';
			$arGroup = 13;
				break;
			case '5':
			$next_level= '2';
			$arGroup = 12;
				break;
			case '0':
			$next_level= '1';
			$arGroup = 11;
				break;
		}
		}
			

	if($next_level){
	 $fields = Array( 
	 "UF_".$next_level."LEVEL_DATE" => date('d.m.Y', strtotime('now')), 
					); 
	 $user = new \CUser;
	// AddMessage2Log($USER_ID);
	$user->Update($USER_ID, $fields);
	$arGroups = \CUser::GetUserGroup($USER_ID);
	$arGroups[] = $arGroup;
	\CUser::SetUserGroup($USER_ID, $arGroups);

					}
			
	}


	public static function getWord($number, $suffix)
	 {
                $keys = array(2, 0, 1, 1, 1, 2);
                $mod = $number % 100;
                $suffix_key = ($mod > 7 && $mod < 20) ? 2: $keys[min($mod % 10, 5)];
                return $suffix[$suffix_key];
     }


    public static function seconds2times($seconds)
	{
	   $times = array();
	   
	   $count_zero = false;
	   
	   $periods = array(60, 3600, 86400, 31536000);
	   
	   for ($i = 3; $i >= 0; $i--)
	   {
	      $period = floor($seconds/$periods[$i]);
	      if (($period > 0) || ($period == 0 && $count_zero))
	      {
	         $times[$i+1] = $period;
	         $seconds -= $period * $periods[$i];
	         
	         $count_zero = true;
	      }
	   }
	   
	   $times[0] = $seconds;
	   return $times;
	}


	public static function checkLoyaltyUser()
	{			
		global $USER;

		$rsUser = \CUser::GetByID($USER->GetID());
		$arUser = $rsUser->Fetch();
				
		
		 unset($discount);
		if($arUser[UF_4LEVEL_DATE]){
			$discount = 20;
		}elseif($arUser[UF_3LEVEL_DATE]){
			$discount = 15;
		}elseif ($arUser[UF_2LEVEL_DATE]) {
			$discount = 10;
		}elseif ($arUser[UF_1LEVEL_DATE]) {
			$discount = 5;
		}elseif($arUser[UF_LOYALTY_ACTIVATE]){
			$discount = '0';
		}

		return $discount;
		 
	}



	public static function lastOrderId()
	{
		global $USER;
		 $dbRes = Order::getList([

			    'filter' => [
			        "USER_ID" => $USER->GetID(),
			        'PAYED' => 'Y',
			    ],
			    'order' => ['ID' => 'DESC']
			]);

			if ($order = $dbRes->fetch())
			{

				$order_id = $order['ID'];

			}
			return $order_id;
	}


	public static function getLoyaltyUserPercent()
	{

		global $USER;

			$dbRes = Order::getList([


			    'filter' => [
			    	"USER_ID" => $USER->GetID(),
			        '!BASKET.PRODUCT_ID' => array("6307", "1628"),
			        '!BASKET.PRICE_TYPE_ID' =>4,
			         'PAYED' => 'Y',
			      
			    ],
			    'order' => ['ID' => 'DESC']
			]);

			$arr = array();
			while ($order = $dbRes->fetch())
			{
			        $arr[$order['ID']] = $order['ID'];
			  
			}

			$orders_count = count($arr);

			$t_4month=10518972;
			$t_7month=18408201;
			$t_10month=26297430;
			$t_13month=34186659;
			 //Оплаченные заказы за все время
			    $dbRes1 = Order::getList([

			    'filter' => [
			        "USER_ID" => $USER->GetID(),
			        'PAYED' => 'Y',
			    ],
			    'order' => ['ID' => 'ASC']
			]);

			// Получение даты первого заказа
			if ($order1 = $dbRes1->fetch())
			{


			// Сколько времени прошло с первого заказа в unix
			$how_time = strtotime('now')-strtotime($order1['DATE_INSERT']);
		
			}
		
			$discount = 0;

			if(($how_time-$t_13month)>0 && $orders_count>20){
			    $fields = Array( 
			"UF_4LEVEL_DATE" => date('d.m.Y',strtotime('now')), 

			); 
			    $discount = 20;
			    }elseif(($how_time-$t_10month)>0 && $orders_count>14){
			$fields = Array( 
			"UF_3LEVEL_DATE" => date('d.m.Y',strtotime('now')), 
			); 
			$discount = 15;

			    }elseif(($how_time-$t_7month)>0 && $orders_count>11){
			$fields = Array( 
			"UF_2LEVEL_DATE" => date('d.m.Y',strtotime('now')), 
			); 
			$discount = 10;

			    }elseif(($how_time-$t_4month)>0 && $orders_count>4){
			$fields = Array( 
			"UF_1LEVEL_DATE" => date('d.m.Y',strtotime('now')), 
			); 
			$discount = 5;

			}else{
				$fields = Array( 
			"UF_LOYALTY_ACTIVATE" => date('d.m.Y',strtotime('now')), 
			); 

			}
			return $fields;


	}


		public static function repeatOldOrder($orderId)
		{
			 \Bitrix\Main\Loader::includeModule('sale');
        \Bitrix\Main\Loader::includeModule("catalog");

        $siteId = Context::getCurrent()->getSite();
        $currencyCode = CurrencyManager::getBaseCurrency();

        $oldOrder = Order::load($orderId);

        $order = Order::create($siteId, $oldOrder->getUserId());
        $order->setPersonTypeId(1);
        $order->setField('CURRENCY', $currencyCode);

        $old_basket = $oldOrder->getBasket();

		$dbRes = \Bitrix\Sale\Basket::getList([
		    'select' => ['PRODUCT_ID', 'QUANTITY'],
		    'filter' => [		    
		     			   '=ORDER_ID' => $orderId,
		       		    ]
		]);
		$Arr_item = array();

		while ($item = $dbRes->fetch())
		{
		   $Arr_item[] = $item;
		      
		}


        $basket = Basket::create($siteId);
      
        
		global $USER;

			$arGroups = $USER->GetUserGroupArray();

		        if ($USER->IsAuthorized()) {
		    // если пользователь принадлежит группе «Постоянные покупатели»
		        if (in_array(8, $arGroups)) {
		        $GROUP_ID = 8;
		    
		    } elseif(in_array(14, $arGroups))
		    {  $GROUP_ID = 14;
		       
		    } elseif(in_array(13, $arGroups))
		    {  $GROUP_ID = 13;
		       
		    }elseif(in_array(12, $arGroups))
		    {  $GROUP_ID = 12;
		       
		    }elseif(in_array(11, $arGroups))
		    {  $GROUP_ID = 11;
		        
		     }elseif(in_array(2, $arGroups))
		    {  $GROUP_ID = 2;
		        
		    }
		}else{
		   $GROUP_ID = 2;
		}



		$db_res = \CCatalogGroup::GetGroupsList(array("GROUP_ID"=>$GROUP_ID, "BUY"=>"Y"));

		while ($ar_res = $db_res->Fetch())
		{
		   $PRICE_TYPE_ID = $ar_res["CATALOG_GROUP_ID"];

			}



	foreach($Arr_item as $item){

			
		 
	  $allProductPrices = \Bitrix\Catalog\PriceTable::getList([
	  "select" => ["*"],
	  "filter" => [
	       "PRODUCT_ID" => $item['PRODUCT_ID'],
	       "CATALOG_GROUP_ID" => $PRICE_TYPE_ID,
	  ],
	   "order" => ["PRICE" => "ASC"]
	])->fetchAll();

	$offer_price = $allProductPrices['0']['PRICE'];

	$quan = intval($item['QUANTITY']);

	$fields = [
		        'PRODUCT_ID' => $item['PRODUCT_ID'],
		        'QUANTITY' => $quan
			  ];

			$r = \Bitrix\Catalog\Product\Basket::addProduct($fields);
	    
	       		 }
   
	}
	
}