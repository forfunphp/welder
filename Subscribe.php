<?
namespace LU\Helpers;

use Bitrix\Currency\CurrencyManager;
use Bitrix\Main\Application;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Context;
use Bitrix\Main\Loader;
use Bitrix\Main\SystemException;
use Bitrix\Main\Type\DateTime;
use Bitrix\Main\UserTable;
use Bitrix\Sale\Basket;
use Bitrix\Sale\Order;
use LU\ORM\SubscribeTable;
use YandexCheckout\Request\Payments\CreatePaymentResponse;
use function Sodium\add;

\Bitrix\Main\Loader::includeModule("sale");
\Bitrix\Main\Loader::includeModule("catalog");
\Bitrix\Main\Loader::includeModule('iblock');

class Subscribe_check
{

   public static function isSubsriberWriteText()
    {
           global $USER;
       
            $isSubscriber = SubscribeTable::getRow([
                'filter' => ['USER' => $USER->GetID(), 'ACTIVE' => 'Y'],
            ]);
         
        
        return $isSubscriber;
    }
}

class Subscribe
{

    const SUBSCRIBE_PRODUCT_ID = 6307;
    const SUBSCRIBE_PAYMENT_ID = 3;
    const SUBSCRIBE_COST = 1750;
    const SUBSCRIBE_DELIVERY_ID = 114;

    static private $isSubscriber;
    static private $fillSubscriber = false;



    public static function notificationAgent()
    {

       


        if( \Bitrix\Main\Loader::includeModule("smsc.sms") and \Bitrix\Main\Loader::includeModule('sale')) {
            $message = "10.02.21 с вашего счета будет оплачена подписка на кофе. Для успешной оплаты потребуется 1750 р на счету.   Для отмены перейдите в личный кабинет https://theweldercatherine.ru/bigbrotherwatchingyou/";
            $sms = new \SMSC_Send(); 

            $subscribers = \LU\ORM\SubscribeTable::getList([
                'filter' => ['ACTIVE' => 'Y', 'UF_SEND_SMS' => true],
                'select' => ['*', 'UF_*']
            ])->fetchAll();

            foreach($subscribers as $subscriber){
                $order = \Bitrix\Sale\Order::load($subscriber['SUBSCRIBE_ORDER_ID']);
                $propertyCollection = $order->getPropertyCollection();
                $phone = '';
                foreach($propertyCollection as $property){
                    if($property->getField('CODE') == 'PHONE'){
                        $phone = $property->getValue();
                    }
                }
                $res = $sms->Send_SMS($phone,$message);
            }
        }
        return "\LU\Helpers\Subscribe::notificationAgent();";
    }

    public static function isSubscriber()
    {
        global $USER;
        if(!self::$fillSubscriber){
            self::$isSubscriber = (bool)SubscribeTable::getRow([
                'filter' => ['USER' => $USER->GetID(), 'ACTIVE' => 'Y'],
            ]);
            self::$fillSubscriber = true;
        }
        return self::$isSubscriber;
    }





    // }

    /**
     * @return string
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     * @throws \Bitrix\Main\ArgumentTypeException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\NotImplementedException
     * @throws \Bitrix\Main\NotSupportedException
     * @throws \Bitrix\Main\ObjectNotFoundException
     * @throws \Bitrix\Main\SystemException
     * @throws \YandexCheckout\Common\Exceptions\ApiException
     * @throws \YandexCheckout\Common\Exceptions\BadApiRequestException
     * @throws \YandexCheckout\Common\Exceptions\ForbiddenException
     * @throws \YandexCheckout\Common\Exceptions\InternalServerError
     * @throws \YandexCheckout\Common\Exceptions\NotFoundException
     * @throws \YandexCheckout\Common\Exceptions\ResponseProcessingException
     * @throws \YandexCheckout\Common\Exceptions\TooManyRequestsException
     * @throws \YandexCheckout\Common\Exceptions\UnauthorizedException
     */
    public static function checkSubscriber()

    {

      
        $subscribers = \LU\ORM\SubscribeTable::getList([
            'filter' => ['ACTIVE' => 'Y', 'UF_CREATED_ORDER' => false],
            'select' =>  ['*', 'UF_*']
        ])->fetchAll();

        foreach($subscribers as $subscriber) {
            if(\DateTime::createFromFormat('y-m-d H:i:s', $subscriber['SUBSCRIBE_NEXT_PAY_DATE'])->getTimestamp() < time()){
                $order = \LU\Helpers\Subscribe::repeatOrder($subscriber['SUBSCRIBE_ORDER_ID']);
                SubscribeTable::update($subscriber['ID'], [
                    'UF_CREATED_ORDER' => 'Y'
                ]); 
                include_once(Application::getDocumentRoot() . "/local/lib/YandexKassa/autoload.php");

                $client = new \YandexCheckout\Client();
                $client->setAuth('695308', 'live_rAatxtuDnIRbQ2yikEmofyv5hYa86DsMtL4DNeJsTos');

                /** @var CreatePaymentResponse $payment */
                $payment = $client->createPayment([
                    "amount" => [
                        "value" => $order->getPrice(),
                        "currency" => "RUB",
                    ],
                    'confirmation' => [
                        'type' => 'redirect',
                        'return_url' => 'https://theweldercatherine.ru/',
                    ],
                    "capture" => true,
                    "description" => "Оплата подписки на кофе",
                    "payment_method_id" => $subscriber['SAVE_PAYMENT_ID'],
                    "metadata" => [
                        "order" => $order->getId(),
                        "subscribe_pay" => true,
                    ],
                ], \LU\Helpers\Subscribe::genUuid());
            }
        }
        return "\LU\Helpers\Subscribe::checkSubscriber();";

    }

    /**
     * @param string $email
     * @param string $name
     *
     * @return mixed
     * @throws \Bitrix\Main\SystemException
     */
    public static function registerUser($email, $name)
    {
        $existUserId = UserTable::getRow([
            'filter' => ['EMAIL' => $email],
            'select' => ['ID'],
        ])['ID'];
        if($existUserId){
            return $existUserId;
        }

        $user = new \CUser();
        $password = randString();
        $fields = [
            'LOGIN' => $email,
            'EMAIL' => $email,
            'PASSWORD' => $password,
            'CONFIRM_PASSWORD' => $password,
            'NAME' => $name,
        ];
        $id = $user->Add($fields);
        if($id){
            return $id;
        }else{
            throw new SystemException($user->LAST_ERROR);
        }
    }

    /**
     * @param int                      $user
     * @param \Bitrix\Main\HttpRequest $request
     *
     * @return \Bitrix\Sale\Order
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     * @throws \Bitrix\Main\ArgumentTypeException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\NotSupportedException
     * @throws \Bitrix\Main\ObjectNotFoundException
     * @throws \Bitrix\Main\SystemException
     */
    public static function createOrder(int $user, \Bitrix\Main\HttpRequest $request)
    {
        global $USER;


            AddMessage2Log("000000000000000010");

        \Bitrix\Main\Loader::includeModule('sale');
        \Bitrix\Main\Loader::includeModule("catalog");

        $siteId = Context::getCurrent()->getSite();
        $currencyCode = CurrencyManager::getBaseCurrency();

        $order = Order::create($siteId, $user);
        $order->setPersonTypeId(1);
        $order->setField('CURRENCY', $currencyCode);


        $basket = Basket::create($siteId);
        $item = $basket->createItem('catalog', \LU\Helpers\Subscribe::SUBSCRIBE_PRODUCT_ID);
        $item->setFields([
            'QUANTITY' => 1,
            'CURRENCY' => $currencyCode,
            'LID' => $siteId,
            'PRICE' => self::SUBSCRIBE_COST,
            'CUSTOM_PRICE' => 'Y',
            'PRODUCT_PROVIDER_CLASS' => '\CCatalogProductProvider',
        ]);

      

        $order->setBasket($basket);


        $shipmentCollection = $order->getShipmentCollection();
        $shipment = $shipmentCollection->createItem();
        $service = \Bitrix\Sale\Delivery\Services\Manager::getById(self::SUBSCRIBE_DELIVERY_ID);
        $shipment->setFields([
            'DELIVERY_ID' => $service['ID'],
            'DELIVERY_NAME' => $service['NAME'],
        ]);
        $shipmentItemCollection = $shipment->getShipmentItemCollection();
        $shipmentItem = $shipmentItemCollection->createItem($item);
        $shipmentItem->setQuantity($item->getQuantity());

        $paymentCollection = $order->getPaymentCollection();
        $payment = $paymentCollection->createItem();
        $paySystemService = \Bitrix\Sale\PaySystem\Manager::getObjectById(\LU\Helpers\Subscribe::SUBSCRIBE_PAYMENT_ID);
        $payment->setFields([
            'PAY_SYSTEM_ID' => $paySystemService->getField("PAY_SYSTEM_ID"),
            'PAY_SYSTEM_NAME' => $paySystemService->getField("NAME"),
            'SUM' => self::SUBSCRIBE_COST,
        ]);

        /** @var \Bitrix\Sale\PropertyValueCollection $propertyCollection */
        $propertyCollection = $order->getPropertyCollection();
        foreach($propertyCollection as $property) {
            if($property->getField('CODE') == 'PHONE'){
                $property->setValue($request['phone']);
            }
            if($property->getField('CODE') == 'EMAIL'){
                $property->setValue($request['email']);
            }
            if($property->getField('CODE') == 'SUBSCRIBE_ADDRESS'){
                $property->setValue($request['address']);
            }
            if($property->getField('CODE') == 'NAME'){
                $property->setValue($request['name']);
            }
            if($property->getField('CODE') == 'CITY'){
                $property->setValue($request['city']);
            }
        }
        $order->doFinalAction(true);
        $result = $order->save();
        if($result->isSuccess()){
            return $order;
        }else{
            throw new SystemException(implode("<br>", $result->getErrorMessages()));
        }
    }

    /**
     * @return string $uuid
     */
    public static function genUuid()
    {
        $uuid = [
            'time_low' => 0,
            'time_mid' => 0,
            'time_hi' => 0,
            'clock_seq_hi' => 0,
            'clock_seq_low' => 0,
            'node' => [],
        ];

        $uuid['time_low'] = mt_rand(0, 0xffff) + (mt_rand(0, 0xffff) << 16);
        $uuid['time_mid'] = mt_rand(0, 0xffff);
        $uuid['time_hi'] = (4 << 12) | (mt_rand(0, 0x1000));
        $uuid['clock_seq_hi'] = (1 << 7) | (mt_rand(0, 128));
        $uuid['clock_seq_low'] = mt_rand(0, 255);

        for($i = 0; $i < 6; $i++) {
            $uuid['node'][$i] = mt_rand(0, 255);
        }

        $uuid = sprintf('%08x-%04x-%04x-%02x%02x-%02x%02x%02x%02x%02x%02x',
            $uuid['time_low'],
            $uuid['time_mid'],
            $uuid['time_hi'],
            $uuid['clock_seq_hi'],
            $uuid['clock_seq_low'],
            $uuid['node'][0],
            $uuid['node'][1],
            $uuid['node'][2],
            $uuid['node'][3],
            $uuid['node'][4],
            $uuid['node'][5]
        );

        return $uuid;
    }

    /**
     * @param \Bitrix\Sale\Order $order
     *
     * @return mixed
     * @throws \YandexCheckout\Common\Exceptions\ApiException
     * @throws \YandexCheckout\Common\Exceptions\BadApiRequestException
     * @throws \YandexCheckout\Common\Exceptions\ForbiddenException
     * @throws \YandexCheckout\Common\Exceptions\InternalServerError
     * @throws \YandexCheckout\Common\Exceptions\NotFoundException
     * @throws \YandexCheckout\Common\Exceptions\ResponseProcessingException
     * @throws \YandexCheckout\Common\Exceptions\TooManyRequestsException
     * @throws \YandexCheckout\Common\Exceptions\UnauthorizedException
     */
    public static function initPay(Order $order)
    {
        include_once(Application::getDocumentRoot() . "/local/lib/YandexKassa/autoload.php");

        $client = new \YandexCheckout\Client();
        $client->setAuth('695308', 'live_rAatxtuDnIRbQ2yikEmofyv5hYa86DsMtL4DNeJsTos');
        /** @var CreatePaymentResponse $payment */

        $payment = $client->createPayment([
            "amount" => [
                "value" => $order->getPrice(),
                "currency" => "RUB",
            ],
            'confirmation' => [
                'type' => 'redirect',
                'return_url' => 'https://theweldercatherine.ru/',
            ],
            "description" => "Оплата подписки на кофе",
            "payment_method_data" => [
                "type" => "bank_card",
            ],
            "capture" => true,
            "save_payment_method" => true,
            "metadata" => [
                "order" => $order->getId(),
                "subscribe_pay" => true,
            ],
        ], self::genUuid());

        return ($payment->_confirmation->_confirmationUrl);

    }

    /**
     * @param $user
     * @param $order
     * @param $paymentId
     *
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function subscribe($user, $order, $paymentId)
    {
        $alreadySubscriber = (bool)SubscribeTable::getRow([
            'filter' => ['USER' => $user, 'ACTIVE' => 'Y'],
        ]);
        if($alreadySubscriber){
            throw new SystemException('Пользователь уже оформил подписку');
        }
        $now = new DateTime();
        $nextPay = new DateTime();
        $nextPay->add('P1M');


/*
        if(date('d') >= 10){
            if(date('d') == 10){
                if(date('H') >= 14 ){
                    $nextPay->add('P1M');
                }
            } else {
                $nextPay->add('P1M');
            }
        }
        */

        if(date('d') >= 11){
            $nextPay->add('P1M');
        }

        SubscribeTable::add([
            'USER' => $user,
            'SUBSCRIBE_ORDER_ID' => $order,
            'SUBSCRIBE_START_DATE' => $now->format('y-m-d H:i:s'),
            'SUBSCRIBE_CANCEL_DATE' => '',
            'SUBSCRIBE_NEXT_PAY_DATE' => $nextPay->format('y-m-10 H:i:s'),
            'SUBSCRIBE_LAST_PAY_DATE' => $now->format('y-m-d H:i:s'),
            'SUBSCRIBE_PRODUCT' => self::SUBSCRIBE_PRODUCT_ID,
            'SAVE_PAYMENT_ID' => $paymentId,
            'ACTIVE' => 'Y',
            'SUBSCRIBE_TEXT' => '',
        ]);
    }

    /**
     * @param $orderId
     *
     * @return \Bitrix\Sale\Order
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     * @throws \Bitrix\Main\ArgumentTypeException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\NotImplementedException
     * @throws \Bitrix\Main\NotSupportedException
     * @throws \Bitrix\Main\ObjectNotFoundException
     * @throws \Bitrix\Main\SystemException
     */
    public static function repeatOrder($orderId)
    {
        \Bitrix\Main\Loader::includeModule('sale');
        \Bitrix\Main\Loader::includeModule("catalog");

        $siteId = Context::getCurrent()->getSite();
        $currencyCode = CurrencyManager::getBaseCurrency();

        $oldOrder = Order::load($orderId);

        $userId = $oldOrder->getUserId();

        $orderData = \Bitrix\Sale\Order::getList([
          'select' => ['ID'],
          'filter' => ['=USER_ID' => $userId],
          'order' => ['ID' => 'DESC'],
          'limit' => 1
        ]);
        if ($orderrepet = $orderData->fetch())
        {
          $oldOrderId = $orderrepet['ID'];
        }

        $oldOrder = Order::load($oldOrderId);

        $order = Order::create($siteId, $oldOrder->getUserId());
        $order->setPersonTypeId(1);
        $order->setField('CURRENCY', $currencyCode);


        $basket = Basket::create($siteId);
        $item = $basket->createItem('catalog', \LU\Helpers\Subscribe::SUBSCRIBE_PRODUCT_ID);
        $item->setFields([
            'QUANTITY' => 1,
            'CURRENCY' => $currencyCode,
            'LID' => $siteId,
            'PRODUCT_PROVIDER_CLASS' => '\CCatalogProductProvider',
        ]);
        $order->setBasket($basket);


        $shipmentCollection = $order->getShipmentCollection();
        $shipment = $shipmentCollection->createItem();
        $service = \Bitrix\Sale\Delivery\Services\Manager::getById(self::SUBSCRIBE_DELIVERY_ID);
        $shipment->setFields([
            'DELIVERY_ID' => $service['ID'],
            'DELIVERY_NAME' => $service['NAME'],
        ]);
        $shipmentItemCollection = $shipment->getShipmentItemCollection();
        $shipmentItem = $shipmentItemCollection->createItem($item);
        $shipmentItem->setQuantity($item->getQuantity());

        $paymentCollection = $order->getPaymentCollection();
        $payment = $paymentCollection->createItem();
        $paySystemService = \Bitrix\Sale\PaySystem\Manager::getObjectById(\LU\Helpers\Subscribe::SUBSCRIBE_PAYMENT_ID);
        $payment->setFields([
            'PAY_SYSTEM_ID' => $paySystemService->getField("PAY_SYSTEM_ID"),
            'PAY_SYSTEM_NAME' => $paySystemService->getField("NAME"),
            'SUM' => self::SUBSCRIBE_COST,
        ]);

        $oldProperties = $oldOrder->getPropertyCollection();
        $request = [];
        foreach($oldProperties as $property) {

            if($property->getField('CODE') == 'PHONE'){
                $request['phone'] = $property->getValue();
            }
            if($property->getField('CODE') == 'SUBSCRIBE_ADDRESS'){
                $request['address'] = $property->getValue();
            }
            if($property->getField('CODE') == 'LOCATION'){
                $request['location'] = $property->getValue();
            }

            if($property->getField('CODE') == 'NAME'){
                $request['name'] = $property->getValue();
            }
            if($property->getField('CODE') == 'CITY'){
                $request['city'] = $property->getValue();
            }
            if($property->getField('CODE') == 'EMAIL'){
                $request['email'] = $property->getValue();
            }
        }

        /** @var \Bitrix\Sale\PropertyValueCollection $propertyCollection */
        $propertyCollection = $order->getPropertyCollection();
        foreach($propertyCollection as $property) {
            if($property->getField('CODE') == 'PHONE'){
                $property->setValue($request['phone']);
            }
            if($property->getField('CODE') == 'SUBSCRIBE_ADDRESS'){
                $property->setValue($request['address']);
            }
             if($property->getField('CODE') == 'LOCATION'){
                $property->setValue($request['location']);
            }
            if($property->getField('CODE') == 'NAME'){
                $property->setValue($request['name']);
            }
            if($property->getField('CODE') == 'CITY'){
                $property->setValue($request['city']);
            }
            if($property->getField('CODE') == 'EMAIL'){
                $property->setValue($request['email']);
            }

        }

        $order->doFinalAction(true);
        $result = $order->save();
        if($result->isSuccess()){
            return $order;
        }else{
            throw new SystemException(implode("<br>", $result->getErrorMessages()));
        }
    }

}
